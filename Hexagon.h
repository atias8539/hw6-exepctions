#include <iostream>
#include "shape.h"
#include "MathFunctions.h"
class Hexagon : public Shape 
{
public:
	Hexagon(std::string nam, std::string col, double size);
	~Hexagon();
	void setSize(double size);
	void draw();
private:
	double _length;

};