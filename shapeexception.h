#pragma once
#include <exception>

class shapeException : public std::exception
{
	virtual const char* what() const
	{
		return "This is a shape exception!\n";
	}
};
class InputException : public std::exception
{
	virtual const char* what() const
	{
		return "You entered invalid number\n";

	}
};