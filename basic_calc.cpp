#include <iostream>
#define unuseNumber 8200
#define invalidNumber -1
bool add(int a, int &b)
{
	bool answer = true;
	if (a == unuseNumber || b == unuseNumber)
	{
		answer = false;
	}
	else
	{
		b += a;

		if (b != unuseNumber)
		{
			answer = false;
		}
	}
	return answer;
}

bool  multiply(int &a, int b)
{
	int sum = 0;
	bool answer = false;
	if (a == unuseNumber || b == unuseNumber)
	{
		answer = false;
	}
	else
	{

		for (int i = 0; i < b; i++)
		{
			if (add(a, sum) || sum == unuseNumber)
			{
				a = sum;
				answer = true;
			}
		}
		a = sum;
	}
	return answer;
}

int pow(int a, int b)
{
	int exponent = 1;
	int answer = 0;
	if (a == unuseNumber || b == unuseNumber)
	{
		exponent = invalidNumber;
	}
	else
	{
		for (int i = 0; i < b; i++)
		{
			if (exponent == unuseNumber || multiply(exponent, a))
			{
				exponent = invalidNumber;
			}
		}
	}
	return exponent;
}
void printing()
{
	std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << std::endl;
}
int main(void) {
	int result = pow(2, 3);
	if (result == invalidNumber)
	{
		printing();
	}
	else
	{
		std::cout << result << std::endl;
	}
	result = pow(1, 8200);
	if (result == invalidNumber)
	{
		printing();
	}
	else
	{
		std::cout << result << std::endl;
	}
	system("pause");
}
