#include <iostream>
#include "MathFunctions.h"
double MathFunctions::CalPentagonArea(double size)
{
	return (sqrt(5 * (5 + 2 * (sqrt(5)))) * size * size) / 4;
}
double MathFunctions::CalHexagonArea(double size)
{
	return ((3 * sqrt(3) * (size * size)) / 2);
}