#include <string>
#include "shapeexception.h"
#include <sstream>
#include "Pentagon.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "Hexagon.h"
int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	double size = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, size);
	Hexagon hexa(nam, col, size);
	Shape *ptrpent = &pent;
	Shape *ptrhexa = &hexa;
	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	double d;

	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	std::string niv;
	while (x != 'x') {

		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram,a=Pentagon,b=Hexagon" << std::endl;
		std::cin >> niv;
		while (niv.size() > 1)
		{
			std::cout << "Warning - Don't try to build more than one shape at once";
			std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram" << std::endl;
			std::cin >> niv;
		}
		shapetype = niv[0];


		try
		{

			switch (shapetype) {
			case 'c':
				try
				{
					std::cout << "enter color, name,  rad for circle" << std::endl;
					std::cin >> col >> nam >> rad;
					if (std::cin.fail())
					{
						std::cin.clear();
						throw InputException();
					}
					if (rad <= 0)
					{
						throw shapeException();
						std::cout << "enter rad for circle" << std::endl;
						std::cin >> rad;
					}

					circ.setColor(col);
					circ.setName(nam);
					circ.setRad(rad);
					ptrcirc->draw();
					break;
				}
				catch (const std::exception& e)
				{

					printf(e.what());
				}
			case 'q':
				try
				{
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					if (std::cin.fail())
					{
						std::cin.clear();
						throw InputException();
					}
					if (height <= 0 || width <= 0)
					{
						throw shapeException();
					}

					quad.setName(nam);
					quad.setColor(col);
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
					break;
				}
				catch (const std::exception& e)
				{
					printf(e.what());
				}
			case 'r':
				try
				{
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					if (std::cin.fail())
					{
						std::cin.clear();
						throw InputException();
					}
					if (height <= 0 || width <= 0)
					{
						throw shapeException();
					}
					rec.setName(nam);
					rec.setColor(col);
					rec.setHeight(height);
					rec.setWidth(width);
					ptrrec->draw();
					break;
				}
				catch (const std::exception& e)
				{
					printf(e.what());
				}
			case 'p':
				try
				{
					std::cout << "enter name, color, height, width, 2 angles" << std::endl;
					std::cin >> nam >> col >> height >> width >> ang >> ang2;
					if (std::cin.fail())
					{
						std::cin.clear();
						throw InputException();
					}
					if (height <= 0 || width <= 0 || ang <= 0 || ang2 <= 0)
					{
						throw shapeException();
					}
					if (ang + ang2 > 180 || ang < 0 || ang2 < 0)
					{
						throw shapeException();
					}
					para.setName(nam);
					para.setColor(col);
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
					ptrpara->draw();
					break;
				}
				catch (const std::exception& e)
				{
					printf(e.what());
				}
			case 'a':
				try
				{
					std::cout << "enter name, color,size" << std::endl;
					std::cin >> nam >> col >> size;
					if (std::cin.fail())
					{
						std::cin.clear();
						throw InputException();
					}
					if (size <= 0)
					{
						throw shapeException();
					}
					pent.setName(nam);
					pent.setColor(col);
					pent.setSize(size);
					ptrpent->draw();
					break;
				}
				catch (const std::exception& e)
				{
					printf(e.what());
				}
				break;
			case 'b':
				try
				{
					std::cout << "enter name, color,size" << std::endl;
					std::cin >> nam >> col >> size;
					if (std::cin.fail())
					{
						std::cin.clear();
						throw InputException();
					}
					if (size <= 0)
					{
						throw shapeException();
					}
					hexa.setName(nam);
					hexa.setColor(col);
					hexa.setSize(size);
					ptrhexa->draw();
					break;
				}
				catch (const std::exception& e)
				{
					printf(e.what());
				}
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cin.clear();
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (std::exception e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



	system("pause");
	return 0;

}