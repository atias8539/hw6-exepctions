#pragma once
#include<string>
#include <iostream>

class MathFunctions
{

public:
	static double CalPentagonArea(double size);
	static double CalHexagonArea(double size);

};