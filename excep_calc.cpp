#include <iostream>
#define unuseNumber 8200
int add(int a, int b)
{
	if (a + b == unuseNumber || a == unuseNumber || b == unuseNumber)
	{
		throw(unuseNumber);
	}
	return a + b;
}

int  multiply(int a, int b)
{
	int sum = 0;
	for (int i = 0; i < b; i++)
	{
		if (a == unuseNumber || b == unuseNumber || i == unuseNumber || sum == unuseNumber)
		{
			throw(unuseNumber);
		}
		sum = add(sum, a);
	}
	return sum;
}

int  pow(int a, int b)
{
	int exponent = 1;
	for (int i = 0; i < b; i++)
	{
		exponent = multiply(exponent, a);
		if (a == unuseNumber || b == unuseNumber || i == unuseNumber || exponent == unuseNumber)
		{
			throw(unuseNumber);
		}
	}
	return exponent;
}

int main(void)
{
	try
	{
		std::cout << pow(4, 5) << std::endl;
		std::cout << pow(1, 8200) << std::endl;
	}
	catch (int unuseNumberFail)
	{
		std::cout << "This user is not authorized to access" << unuseNumberFail << ", please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	getchar();
	return 0;
}