#include "Hexagon.h"
#include "shapeexception.h"
Hexagon::Hexagon(std::string nam, std::string col, double size) : Shape(nam, col)
{
	this->_length = size;
}
Hexagon::~Hexagon()
{
}

void Hexagon::draw()
{
	double area = MathFunctions::CalHexagonArea(this->_length);
	std::cout << getName() <<"\n"<<getColor() << "\n" << "the size of hexagon is: " << _length << "\n" << "the area of hexagon is " << area << std::endl;
}
void Hexagon::setSize(double size)
{
	if (size < 0)
	{
		throw InputException();
	}
	else
	{
		this->_length = size;
	}
}