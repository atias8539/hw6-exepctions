#include "shape.h"
#include <iostream>
#include "MathFunctions.h"
class Pentagon : public Shape {
public:
	Pentagon(std::string nam, std::string col, double size);
	~Pentagon();
	void setSize(double size);
	void draw();
private:
	double _length;

};
