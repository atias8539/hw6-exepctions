#include "Pentagon.h"
#include "shapeexception.h"
Pentagon::Pentagon(std::string nam, std::string col, double size) : Shape(nam, col)
{
	this->_length = size;
}
Pentagon::~Pentagon()
{
}
void Pentagon::setSize(double size)
{
	if (size < 0)
	{
		throw InputException();
	}
	else
	{
		this->_length = size;
	}
}
void Pentagon::draw()
{
	double area = MathFunctions::CalPentagonArea(this->_length);
	std::cout << getName() << "\n" << "Color is " << getColor() <<"\n"<< "the size of pentagon is " << _length << std::endl << "the area of pentagon is " <<area << std::endl;
}